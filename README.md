<img src="https://gitlab.com/InfoKnow/CloudFogComputing/mults/raw/master/utils/mults-logo.png" height="200"/>

MULTS - A lightweight **Mul**ti-cloud Architecture for **T**ransient **S**ervers.
=============


## Project Website
Please, visit our website for detailed information and how to use our framework.

http://mults.cic.unb.br


## Related Projects
:warning: Soon...


## License
[Apache 2.0](http://www.apache.org/licenses/LICENSE-2.0.txt)